#!/bin/bash
#!/bin/sh

echo "Starting to hide the SNPs range from 5% to 60% randomly....."
cd ./dataGenerate/

matlab -nodesktop -nojvm -nodisplay -nosplash  -r generate_hidden_SNPs_ihmm #1>running.log 2>running.err #&

#rm running.log running.err

echo "Starting to infer the hidden SNPs with iHMM....."

cd ../iHMM_infer/
./ihmm_inference.sh



echo "Starting to evaluate the iHMM inferred results....."
cd ../evaluation/

matlab -nodesktop -nojvm -nodisplay -nosplash  -r evaluate_ihmm #1>running.log 2>running.err #&

#rm running.log running.err

echo "iHMM is done....."
cd ../




