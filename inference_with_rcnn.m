disp("Starting to hide the SNPs range from 5% to 60% randomly.....");
cd ./dataGenerate/
generate_hidden_SNPs_rcnn 

disp("Starting to infer the hidden SNPs with RCNN.....");

cd ../RCNN_infer/
RCNN_infer_handle

disp("Starting to evaluate the RCNN inferred results.....");
cd ../evaluation/

evaluate_rcnn

disp("RCNN is done.....");
cd ../