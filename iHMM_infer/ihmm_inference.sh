#!/bin/bash

for file in `ls ../hapmap/small_data_ihmm/hidden_SNPs/*.gen.txt`;

do
    phasedfile=${file/%.gen.txt/.phased}
    hapsfile=${phasedfile}'_haps'
    inferred_file=${file/%.gen.txt/.inferred.txt}
    echo "Processing file: " ${file} 
    sed -i 's/-1/?/g' ${file}
    
    `./impute/impute \
     -prephase_g \
     -m ../hapmap/small_data_ihmm/genetic_map_chr22_combined_b36.txt \
     -g ${file} \
     -int 14.87e6 17.5e6 \
     -Ne 20000 \
     -o ${phasedfile}
    `
    
    `./impute/impute \
    -use_prephased_g \
     -m ../hapmap/small_data_ihmm/genetic_map_chr22_combined_b36.txt \
     -h ../hapmap/small_data_ihmm/small_chr22_CEU_ref.haps.txt \
     -l ../hapmap/small_data_ihmm/small_chr22_CEU_ref.legend.txt \
     -known_haps_g  ${hapsfile} \
     -strand_g ../hapmap/small_data_ihmm/small_chr22_genotypes_inputs.strand.txt \
     -int 14.87e6 17.5e6 \
     -Ne 20000 \
     -o ${inferred_file}`
    mv  ${hapsfile} ${hapsfile/%_haps/.haps.txt} 
    rm ${phasedfile}
    
    sed -i 's/?/-1/g' ${file}
done

rm ../hapmap/small_data_ihmm/hidden_SNPs/*.txt_*
rm ../hapmap/small_data_ihmm/hidden_SNPs/*.phased_*
