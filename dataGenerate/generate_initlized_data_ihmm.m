clear



new_gens=readtable('../hapmap/new_genotypes_chr22_CEU.txt');
%[C,ia,ib]=intersect(table2array(gens(:,4)), table2array(new_gens(:,1)));
% all the new gens are generated from gens, and included in gens.
gens = readtable('../hapmap/chr22/small_genotypes_chr22_CEU.txt');
haps =readtable('../hapmap/chr22/small_CEU.chr22.hap.txt');

legend=readtable('../hapmap/hapmap3_r2_b36/hapmap3_r2_b36_chr22.legend.txt');
%haps =readtable('../hapmap/hapmap3_r2_b36/hapmap3_r2_b36_chr22_all.haps.txt');
map=readtable('../hapmap/hapmap3_r2_b36/genetic_map_chr22_combined_b36.txt');
[C,ia,ib]=intersect(table2array(gens(:,1)), table2array(legend(:,2)));

gens=gens(ia,:);
small_gens=table2array(gens(1:1000,2:end));

legend=legend(ib,:);
small_lagend=legend(1:1000,:);

[C,ia,ib]=intersect(table2array(gens(:,1)), table2array(haps(:,1)));


haps=haps(ib,:);
small_haps=haps(1:1000,2:end);
writetable(small_lagend,'../hapmap/small_data_ihmm/small_chr22_CEU_ref.legend.txt','Delimiter',' ','WriteVariableNames',1);
writetable(small_haps,'../hapmap/small_data_ihmm/small_chr22_CEU_ref.haps.txt','Delimiter',' ','WriteVariableNames',0);

extened_gens=zeros(size(small_gens,1),3*size(small_gens,2));
for i=1:size(small_gens,1)
    for j=1:size(small_gens,2)
        if small_gens(i,j)==-1
            extened_gens(i,(3*j-2):(3*j))=-1;
        else
            extened_gens(i,(3*j-2)+small_gens(i,j))=1;
        end
    end
end
genformat_gens=[table(22*(zeros(size(small_gens,1),1)+1)), small_lagend, table(extened_gens)];
writetable(genformat_gens,'../hapmap/small_data_ihmm/small_chr22_genotypes_inputs.gen.txt','Delimiter',' ','WriteVariableNames',0);           

genformat_strand=small_lagend(:,2:3);
genformat_strand(:,2)={'+'};

large_gens=readtable('../hapmap/genotypes_chr22_CEU_phase3.3_consensus.b36_fwd.txt');
[C,ia,ib]=intersect(table2array(large_gens(:,4)), table2array(genformat_strand(:,1)));
genformat_strand(ib,2)=large_gens(ia,5);

writetable(genformat_strand,'../hapmap/small_data_ihmm/small_chr22_genotypes_inputs.strand.txt','Delimiter',' ','WriteVariableNames',0);



% hap=readtable('../hapmap/chr22/small_CEU.chr22.hap.txt');
% [C,ia,ib]=intersect(table2array(hap(:,1)), table2array(small_lagend(:,2)));
% hap=hap(ia,:);
% temp=small_haps(ib,:);
% individuals=zeros(1,250);
% k=1;
% for i=2:235
%     diff=zeros(1,2022);
%     for j=1:2022
%         diff(j)=sum(table2array(hap(:,i))==table2array(temp(:,j)));
%     end
%     individuals(k)=find(diff==max(diff));
%     k=k+1;
% end


% [C,ia,ib]=intersect(table2array(map(:,1)), table2array(small_lagend(:,2)));








% [C,ia,ib]=intersect(table2array(haps(:,1)), table2array(new_gens(:,1)));
% 
% 
% map=readtable('../hapmap/hapmap3_r2_b36/genetic_map_chr22_combined_b36.txt');
% 
% 
% %
% 
% [C,ia,ib]=intersect(table2array(gens(:,4)), table2array(haps(:,1)));
% 
% gens=gens(ia,:);
% 
% individualCount=size(gens,2)-11;
% SNPsCount=size(gens,1);
% 
% simple_gens=zeros(SNPsCount,individualCount);
% simple_haps=zeros(SNPsCount,2*individualCount);
% genformat_gens=zeros(SNPsCount,3*individualCount);
% for i=1:SNPsCount
%     allels=split(table2array(gens(i,2)),'/');
%     for j=1:individualCount
%         genotype=string(table2array(gens(i,11+j)));
%         if startsWith(genotype,string(allels(1)))
%             simple_haps(i,2*j-1)=0;
%         elseif startsWith(genotype,string(allels(2)))
%             simple_haps(i,2*j-1)=1;
%         else
%             simple_haps(i,2*j-1)=-1;
%         end
%         if endsWith(genotype,string(allels(1)))
%             simple_haps(i,2*j)=0;
%         elseif endsWith(genotype,string(allels(2)))
%             simple_haps(i,2*j)=1;
%         else
%             simple_haps(i,2*j)=-1;
%         end
%         if simple_haps(i,2*j)==-1 | simple_haps(i,2*j-1) ==-1
%             simple_gens(i,j)=-1;
%             genformat_gens(i,3*j-2)=-1;
%             genformat_gens(i,3*j-1)=-1;
%             genformat_gens(i,3*j)=-1;
%         else
%             simple_gens(i,j)= simple_haps(i,2*j-1)+simple_haps(i,2*j);
%             genformat_gens(i,3*j-2+simple_gens(i,j))=1;
%         end
%     end
% end
% 
% 
