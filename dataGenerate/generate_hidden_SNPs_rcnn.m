%clear

haps=readtable('../hapmap/small_data_rcnn/small_chr22_genotypes_inputs.haps.txt');

for percentage=0.05:0.05:0.6
    hidden_haps=haps;
    hidden_position=randperm(size(haps,1),int32(size(haps,1)*percentage));
    hidden_haps(hidden_position,2:end)={-1};
    hidden_file=['../hapmap/small_data_rcnn/hidden_SNPs/small_chr22_genotypes_','per',mat2str(percentage*100),'.haps.txt'];
    writetable(hidden_haps,hidden_file,'Delimiter',' ','WriteVariableNames',0);
end

%exit
