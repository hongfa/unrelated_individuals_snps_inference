clear

original_gen_file=['../hapmap/small_data_ihmm/small_chr22_genotypes_inputs.gen.txt'];
gens=readtable('../hapmap/small_data_ihmm/small_chr22_genotypes_inputs.gen.txt');

for percentage=0.05:0.05:0.6
    hidden_gens=gens;
    hidden_position=randperm(size(gens,1),int32(size(gens,1)*percentage));
    hidden_gens(hidden_position,6:end)={-1};

%      % hidden different position for different individuals
%      for i=1:(size(hidden_gens,2)-5)/3
%         hidden_position=randperm(size(gens,1),int32(size(gens,1)*percentage));
%         hidden_gens(hidden_position,5+(3*i-2))={-1};
%         hidden_gens(hidden_position,5+(3*i-1))={-1};
%         hidden_gens(hidden_position,5+(3*i))={-1};
%     end
    hidden_file=['../hapmap/small_data_ihmm/hidden_SNPs/small_chr22_genotypes_','per',mat2str(percentage*100),'.gen.txt'];
    writetable(hidden_gens,hidden_file,'Delimiter',' ','WriteVariableNames',0);
end
exit
