
%% 
%reference_haps: the haplotype set from on specific population, the first
%cloumn is the position, the 2:end column are the haplotypes of 2:end
%sample individauls of this population.
%
%hidden_haps: the checking haplotypes of different individuals, the
%format is same to reference_haps.
%
%hidden_positon: the hidden position of the individuals' haplotypes, these haplotypes
%need to be infered.
function [inferred_SNPs, inferred_SNPs_probs]=RCNN_SNPs_infer(reference_haps,hidden_haps,iterations)

reference_count=size(reference_haps,2);
haps_count=size(reference_haps,1);
train_referenc_count=int32(reference_count*0.9);
test_referenc_count=reference_count-train_referenc_count;
train_data=reference_haps(:,1:train_referenc_count);
test_data=reference_haps(:,train_referenc_count+1:end);
hidden_position=find(hidden_haps(:,1)==-1);
randnumber=hidden_position;

%train_data=population_haps(:,2:end);
%test_data=family_haps(:,2:end);

data=train_data;
datax=train_data;
datax(randnumber,:)=[];
datay=data(randnumber,:);

%trainx=datax; trainx=reshape(trainx,[2295-229, 1,1,2022]);
trainx=datax; 
trainx=reshape(trainx,[haps_count-length(randnumber),1,1,train_referenc_count]);

trainy=datay';

datatrain=test_data; datatrain(randnumber,:)=[];
%testx=datatrain; testx=reshape(testx,[2295-229, 1,1,34]);
testx=datatrain; 
testx=reshape(testx,[haps_count-length(randnumber), 1,1,test_referenc_count]);
testy=test_data(randnumber,:);

%% Regression CNN
layers = [imageInputLayer([haps_count-length(randnumber) 1 1]);
          convolution2dLayer(1,25);
          reluLayer();
          crossChannelNormalizationLayer(1)
          maxPooling2dLayer(1,'Stride',2);
%           convolution2dLayer(1,1);
%           reluLayer();
          fullyConnectedLayer(length(randnumber));
          regressionLayer()];

      
%options = trainingOptions('sgdm','MaxEpochs',1000,'InitialLearnRate',0.001,'ExecutionEnvironment','parallel');
options = trainingOptions('sgdm','MaxEpochs',iterations,'InitialLearnRate',0.001);
[net, info] = trainNetwork(trainx,trainy,layers,options);
% figure,
% plot(info.TrainingRMSE,'k-d')
 
 
y_predicted = predict(net,testx);
%save gene_half.mat net y_predicted info randnumber trainx trainy testx testy
%% Results processing
results=y_predicted;
%  results(find(y_predicted<0.6))=0;
%   results(find(y_predicted>0.6))=1;
% [m n]=find(results~=testy);
% accuracy=1-size(n,1)/(size(testy,1)*size(testy,2))

%% choose the best threshold
k=1;
for i=0.1:0.005:1
	results(find(y_predicted<i))=0;
	results(find(y_predicted>i))=1;
    [m n]=find(results'~=testy);
    accuracy(k)=1-size(n,1)/(size(testy,1)*size(testy,2));
    k=k+1;
end
[m n]=max(accuracy);
thre=0.1:0.005:1; 
bestthre=thre(n);
haps_train_accuracy=m;


%% checking the network by using the best threshold 
checkx=hidden_haps;
checky=hidden_haps(randnumber,:);

checkx(randnumber,:)=[];
checkx=reshape(checkx,[haps_count-length(randnumber), 1,1,size(checkx,2)]);
check_y_predicted = predict(net,checkx);


check_y_result=check_y_predicted;
check_y_result(find(check_y_predicted<bestthre))=0;
check_y_result(find(check_y_predicted>=bestthre))=1;
[m n]=find(check_y_result'~=checky);
check_accuracy=1-size(n,1)/(size(checky,1)*size(checky,2));

infer_result=check_y_result';
haps_infer_accuracy=check_accuracy;

%check_y_prob=prob(check_y_predicted,bestthre);
check_y_prob=check_y_predicted; %the probalities of check_y==1
a=1.2^40;
for i=1:size(check_y_prob,1)
    for j=1:size(check_y_prob,2)
        if check_y_predicted(i,j)>bestthre
            %check_y_prob(i,j)=0.5+ (check_y_predicted(i,j)-bestthre)/(max(check_y_predicted(i,:))-bestthre)*0.5;
            x=check_y_predicted(i,j);
            c=(a-1)/(max(check_y_predicted(i,:))-bestthre);
            check_y_prob(i,j)=log((x-bestthre)*c+1)/log(1.2)/80+0.5;
            if check_y_prob(i,j)>1
                check_y_prob(i,j)=1;
            end 
        elseif check_y_predicted(i,j)<bestthre
            %check_y_prob(i,j)=0.5-(bestthre-check_y_predicted(i,j))/(bestthre-min(check_y_predicted(i,:)))*0.5;
            x=check_y_predicted(i,j);
            c=40/(bestthre-min(check_y_predicted(i,:)));
            mi=min(check_y_predicted(i,:));
            check_y_prob(i,j)=(2.^((x-mi)*c-3)-(2.^(-3)))/(2^((bestthre-mi)*c-3)-(2^(-3)))/2;
        else
            check_y_prob(i,j)=0.5;
        end
    end
end


 

inferred_SNPs=zeros(size(hidden_haps,1),size(hidden_haps,2)/2);
inferred_SNPs_probs=zeros(3,size(hidden_haps,1),size(hidden_haps,2)/2);
gens_probs=zeros(3,size(check_y_result,1)/2,size(check_y_result,2));
for i=1:size(hidden_haps,2)/2
    gens_probs(3,i,:)=check_y_prob((2*i-1),:).*check_y_prob((2*i),:);
    gens_probs(2,i,:)=(1-check_y_prob((2*i-1),:)).*check_y_prob((2*i),:)+check_y_prob((2*i-1),:).*(1-check_y_prob((2*i),:));
    gens_probs(1,i,:)=(1-check_y_prob((2*i-1),:)).*(1-check_y_prob((2*i),:));
    
    for j=1:size(hidden_haps,1)
        inferred_SNPs(j,i)=sum(hidden_haps(j,2*i-1:2*i));
        if hidden_haps(j,2*i-1) ~= -1
            inferred_SNPs_probs(inferred_SNPs(j,i)+1,j,i)=1;
        end
    end
    for j=1:length(randnumber)
        inferred_SNPs(randnumber(j),i)=sum(check_y_result((2*i-1):(2*i),j));
        inferred_SNPs_probs(:,randnumber(j),i)=gens_probs(:,i,j);
    end
end
inferred_SNPs;
inferred_SNPs_probs;
