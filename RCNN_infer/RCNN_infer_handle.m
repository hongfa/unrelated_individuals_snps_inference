% clear
% clc

reference_haps=readtable('../hapmap/small_data_rcnn/small_CEU.chr22.hap.txt');
reference_haps=table2array(reference_haps(:,2:end));
file_path=['../hapmap/small_data_rcnn/hidden_SNPs/'];
iterations=1000;
for percentage=0.05:0.05:0.6
    hidden_haps_file=['small_chr22_genotypes_per',mat2str(percentage*100),'.haps.txt'];
    hidden_haps=readtable([file_path hidden_haps_file]);
    hidden_haps=table2array(hidden_haps(:,2:end));
    
    [inferred_SNPs, inferred_probs]=RCNN_SNPs_infer(reference_haps,hidden_haps,iterations);
    iterations=iterations+150;
    inferred_SNPs_file=['small_chr22_genotypes_per',mat2str(percentage*100),'.inferred.mat'];
    inferred_SNPs_probs_file=['small_chr22_genotypes_per',mat2str(percentage*100),'.inferred_probs.mat'];
    save([file_path inferred_SNPs_file],'inferred_SNPs');
    save([file_path inferred_SNPs_probs_file],'inferred_probs');
    
    
end

%exit





