function check_pro=prob(results, bestthre)
mi0=results;
points1=0.2;
points2=bestthre;
points3=0.8;
mi0(results<points1)=mapminmax( mi0(results<points1)',0, 0.1);
mi0(results>=points1 & results < points2)=mapminmax( mi0(results>=points1 & results < points2)',0.1, 0.5);
mi0(results>=points2 & results < points3)=mapminmax( mi0(results>=points2 & results < points3)',0.5, 0.995);
mi0(results>=points3)=mapminmax(mi0(results>=points3)',0.995, 1);
%figure,hist(mi0)
check_pro=mi0;
end

