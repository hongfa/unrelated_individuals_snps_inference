% clear
% clc
original_gens=readtable('../hapmap/small_genotypes_chr22_CEU.txt');
original_gens=table2array(original_gens(:,2:end));

AF=readtable('../hapmap/small_allele_freqs_chr22_CEU.txt');
AF=table2array([AF(:,3) AF(:,5)]);
MAF=[AF(:,1).*AF(:,1) 2*AF(:,1).*AF(:,2) AF(:,2).*AF(:,2)];

filePath=['../hapmap/small_data_rcnn/hidden_SNPs/'];
accuracy=zeros(1,length(0.05:0.05:0.6));
uncertainty=zeros(1,length(0.05:0.05:0.6));
privacy_loss=zeros(1,length(0.05:0.05:0.6));
k=1;
for percentage=0.05:0.05:0.6
    hidden_haps_file=['small_chr22_genotypes_per',mat2str(percentage*100),'.haps.txt'];
    inferred_SNPs_file=['small_chr22_genotypes_per',mat2str(percentage*100),'.inferred.mat'];
    inferred_probs_file=['small_chr22_genotypes_per',mat2str(percentage*100),'.inferred_probs.mat'];
    hidden_haps=readtable([filePath hidden_haps_file]);
    hidden_haps=table2array(hidden_haps(:,2:end));
    reformat_hidden_SNPs=original_gens;
    
    load([filePath inferred_SNPs_file],'inferred_SNPs');
    inferred_SNPs;
    load([filePath inferred_probs_file],'inferred_probs');
    inferred_SNPs_probs=inferred_probs;
    for SNPIdx=1:size(original_gens,1)
        for idividualIdx=1:size(original_gens,2)
            if hidden_haps(SNPIdx,2*idividualIdx)==-1
                 reformat_hidden_SNPs(SNPIdx,idividualIdx)=-1;
            end
        end
    end
    [accuracy(k), uncertainty(k), privacy_loss(k)]=evaluate(original_gens,reformat_hidden_SNPs,inferred_SNPs,MAF,inferred_SNPs_probs);
    k=k+1;
end

%accuracy, uncertainty, privacy_loss
results=[accuracy;uncertainty;privacy_loss]
writetable(table(results),'rcnn_evaluations.txt','Delimiter',' ','WriteVariableNames',0);
% save rcnn_evaluations.mat accuracy  uncertainty  privacy_loss;


%exit
 