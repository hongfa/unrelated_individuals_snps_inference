clear
clc
original_gens=readtable('../hapmap/small_genotypes_chr22_CEU.txt');
original_gens=table2array(original_gens(:,2:end));

AF=readtable('../hapmap/small_allele_freqs_chr22_CEU.txt');
AF=table2array([AF(:,3) AF(:,5)]);
MAF=[AF(:,1).*AF(:,1) 2*AF(:,1).*AF(:,2) AF(:,2).*AF(:,2)];

filePath=['../hapmap/small_data_ihmm/hidden_SNPs/'];
accuracy=zeros(1,length(0.05:0.05:0.6));
uncertainty=zeros(1,length(0.05:0.05:0.6));
privacy_loss=zeros(1,length(0.05:0.05:0.6));
k=1;
for percentage=0.05:0.05:0.6
    hidden_SNPs_file=['small_chr22_genotypes_per',mat2str(percentage*100),'.gen.txt'];
    inferred_SNPs_file=['small_chr22_genotypes_per',mat2str(percentage*100),'.inferred.txt'];
    hidden_SNPs=readtable([filePath hidden_SNPs_file]);
    hidden_SNPs=table2array(hidden_SNPs(:,6:end));
    reformat_hidden_SNPs=original_gens;
    
    inferred_SNPs=readtable([filePath inferred_SNPs_file]);
    inferred_SNPs=table2array(inferred_SNPs(:,6:end));
    reformat_inferred_SNPs=original_gens;
    inferred_SNPs_probs=zeros(3,size(original_gens,1),size(original_gens,2));
    for SNPIdx=1:size(original_gens,1)
        for idividualIdx=1:size(original_gens,2)
            if hidden_SNPs(SNPIdx,3*idividualIdx)==-1
                 reformat_hidden_SNPs(SNPIdx,idividualIdx)=-1;
                 predict_probs=inferred_SNPs(SNPIdx,(3*idividualIdx-2):(3*idividualIdx));
                 
                 genotype=find(predict_probs==max(predict_probs))-1;
                 if size(genotype,2)>1
                     predict_genotype=randperm(size(genotype,2),1)-1;
                 else
                     predict_genotype=genotype;
                 end
                 
                 reformat_inferred_SNPs(SNPIdx,idividualIdx)=predict_genotype;
            end
            inferred_SNPs_probs(:,SNPIdx,idividualIdx)=inferred_SNPs(SNPIdx,(3*idividualIdx-2):(3*idividualIdx));
       end
    end
    [accuracy(k), uncertainty(k), privacy_loss(k)]=evaluate(original_gens,reformat_hidden_SNPs,reformat_inferred_SNPs,MAF,inferred_SNPs_probs);
    k=k+1;
end
results=[accuracy;uncertainty;privacy_loss]
writetable(table(results),'ihmm_evaluations.txt','Delimiter',' ','WriteVariableNames',0);
exit
 