%
% Evaluate the inferred results with three terms, average accuracy,
% averagenormalized uncertainty(entropy), and average normalized privacy
%  loss(mumual information).
%
function [accuracy, uncertainty, privacy_loss]=evaluate(originalSNPs,hiddenSNPs,predictedSNPs,MAFs,predictedProbs)
    accuracies=zeros(1,size(hiddenSNPs,2));
    entropies=zeros(1,size(hiddenSNPs,2));
    privacyLosses=zeros(1,size(hiddenSNPs,2));

    for individualIdx=1:size(hiddenSNPs,2)
        hiddenCnt=0;
        individualError=0;
        individual_entropy=0;
        individual_privacy_loss=0;
        for SNPsIdx =1:size(hiddenSNPs,1)
            if hiddenSNPs(SNPsIdx,individualIdx) == -1
                hiddenCnt= hiddenCnt  + 1;
                if predictedSNPs(SNPsIdx,individualIdx) ~= originalSNPs(SNPsIdx,individualIdx)
                    individualError=individualError+1;
                end
                %if max(predictedProbs(:,SNPsIdx,individualIdx))<1.0
                    predicted_ent=0;
                    maf_ent=0;
                    for i =1:3
                        if predictedProbs(i,SNPsIdx,individualIdx)>0 
                            predicted_ent = predicted_ent- predictedProbs(i,SNPsIdx,individualIdx)*log2(predictedProbs(i,SNPsIdx,individualIdx));
                        end
                        if MAFs(SNPsIdx,i)>0
                            maf_ent = maf_ent - MAFs(SNPsIdx,i)*log2(MAFs(SNPsIdx,i));
                        end
                    end
                    pri_loss=maf_ent-predicted_ent;
                    individual_entropy =  individual_entropy + predicted_ent/log2(3);
                    individual_privacy_loss = individual_privacy_loss + pri_loss/log2(3);
                %end
            end
        end
        accuracies(individualIdx)=1- individualError/hiddenCnt;
        entropies(individualIdx)=individual_entropy/hiddenCnt;
        privacyLosses(individualIdx)=individual_privacy_loss/hiddenCnt;
    end
    accuracy = mean(accuracies);
    uncertainty = mean(entropies);
    privacy_loss = mean(privacyLosses);
end
