#!/bin/bash
#!/bin/sh

for number in {10..20}
do
    echo "The $number time(s) for RCNN based SNPs inference....."
    sh ./inference_with_rcnn.sh
    mv ./evaluation/rcnn_evaluations.txt ./evaluation/rcnn_evaluations_${number}.txt
done

