#!/bin/bash
#!/bin/sh

echo "Starting to hide the SNPs range from 5% to 60% randomly....."
cd ./dataGenerate/

matlab -nodesktop -nojvm -nodisplay -nosplash  -r generate_hidden_SNPs_rcnn #1>running.log 2>running.err #&

#rm running.log running.err

echo "Starting to infer the hidden SNPs with RCNN....."

cd ../RCNN_infer/
matlab -nodesktop -nojvm -nodisplay -nosplash  -r RCNN_infer_handle #1>running.log 2>running.err #&


echo "Starting to evaluate the RCNN inferred results....."
cd ../evaluation/

matlab -nodesktop -nojvm -nodisplay -nosplash  -r evaluate_rcnn #1>running.log 2>running.err #&

#rm running.log running.err

echo "RCNN is done....."
cd ../




