#!/bin/bash
#!/bin/sh

for number in {1..2}
do
    echo "The $number time(s) for iHMM based SNPs inference....."
    sh ./inference_with_ihmm.sh
    mv ./evaluation/ihmm_evaluations.txt ./evaluation/ihmm_evaluations_${number}.txt
done

