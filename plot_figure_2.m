clear
%% figuure error 1

addpath('./evaluation_result/')
pbld=zeros(3,12);
mc2=zeros(3,12);
rm=zeros(3,12);
ihmm=zeros(3,12);
rcnn=zeros(3,12);

for i=1:10
   pbld_file=['PubLD_', num2str(i),'.txt'];
   pbld=pbld+table2array(readtable(pbld_file));

   mc2_file=['MC2_', num2str(i),'.txt'];
   mc2=mc2+table2array(readtable(mc2_file));

   rm_file=['recombination_', num2str(i),'.txt'];
   rm=rm+table2array(readtable(rm_file));

   ihmm_file=['ihmm_evaluations_', num2str(i),'.txt'];
   ihmm=ihmm+table2array(readtable(ihmm_file));

   rcnn_file=['rcnn_evaluations_', num2str(i),'.txt'];
   rcnn=rcnn+table2array(readtable(rcnn_file));
end
pbld=pbld./10;
mc2=mc2./10;
rm=rm./10;
ihmm=ihmm./10;
rcnn=rcnn./10;


figure;
plot(1-pbld(1,:),'b-+')
hold on;
plot(1-mc2(1,:),'r-*')
hold on;
plot(1-rm(1,:),'g-s')
hold on;
plot(1-ihmm(1,:),'c-o')
hold on;
plot(1-rcnn(1,:),'m-v')
hold on;

xlabels = {'0%','10%','20%', '30%','40%','50%','60%'};
%xlabels = {'5%','10%','15%','20%','25%', '30%', '35%','40%','45%', '50%','55%','60%'};
set(gca,'XtickLabel',xlabels(1:7))
title('Error rate');
xlabel('Hidden SNPs percentage');
ylabel('Estimation Error');
legend('PubLD(samani et al)','CM2(samani et al)','RM(samani et al)','iHMM(Ours)','RCNN(Ours)')
ylim( [0,0.6] );


%% uncertainty
figure;
plot(pbld(2,:),'b-+')
hold on;
plot(mc2(2,:),'r-*')
hold on;
plot(rm(2,:),'g-s')
hold on;
plot(ihmm(2,:),'c-o')
hold on;
plot(rcnn(2,:),'m-v')
hold on;

xlabels = {'0%','10%','20%', '30%','40%','50%','60%'};
%xlabels = {'5%','10%','15%','20%','25%', '30%', '35%','40%','45%', '50%','55%','60%'};
set(gca,'XtickLabel',xlabels(1:7))
title('Uncertainty');
xlabel('Hidden SNPs percentage');
ylabel('Average Normalized Entropy');
legend('PubLD(samani et al)','CM2(samani et al)','RM(samani et al)','iHMM(Ours)','RCNN(Ours)')
ylim( [0,0.8] );

figure;
plot(pbld(3,:),'b-+')
hold on;
plot(mc2(3,:),'r-*')
hold on;
plot(rm(3,:),'g-s')
hold on;
plot(ihmm(3,:),'c-o')
hold on;
plot(rcnn(3,:),'m-v')
hold on;

xlabels = {'0%','10%','20%', '30%','40%','50%','60%'};
%xlabels = {'5%','10%','15%','20%','25%', '30%', '35%','40%','45%', '50%','55%','60%'};
set(gca,'XtickLabel',xlabels(1:7))
title('Privacy Loss');
xlabel('Hidden SNPs percentage');
ylabel('Average Normalized Mutual Information');
legend('PubLD(samani et al)','CM2(samani et al)','RM(samani et al)','iHMM(Ours)','RCNN(Ours)')
ylim( [0,0.8] );


